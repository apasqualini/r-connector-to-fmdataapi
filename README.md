# R connector to Filemaker Data API
Ce snippet R permet de vous connecter à une base filemaker et plus spécifiquement à un formulaire via la Data API. Suite à la connexion vous serez en mesure de récupérer l'ensemble des enregistrements disponibles ou d'effectuer une recherche. Les résultats sont retournés au format JSON. Pour exploiter tout le potentiel de la DATA API de Filemaker, veuillez consulter la documentation de filemaker server : https://help.claris.com/en/data-api-guide/content/index.html 

## Getting started

Ouvrez avec R studio ou intégrez le fichier connector.R à votre projet R

Les variables suivantes sont à modifier :
- {yourDomain} => l'adresse de votre serveur filemaker
- {Database Name} => le nom de votre base de données filemaker
- {Login:Password} => les identifiants de l'utilisateur filemaker possédant les droits adéquats
- {LayoutName} => le nom de votre formulaire dans la base de données filemaker qui sera interrogé

Le snippet contient deux choix step4 et 5 dont vous pouvez commenter et décommenter les lignes selon vos besoins :
- le premier choix permet de récupérer les enregistrement disponibles
- le deuxième choix permet de contruire une requête de recherche et de récupérer les résultats renvoyées

## Prérequis

- Vous devez posséder un FM serveur (v19 min.)
- les requêtes fonctionnent avec la v1 et 2 de la data API
- La Data API doit être active sur FM serveur
- L'utilisateur doit être déclaré dans la base de donnée FM (avec les droits sur DATA API) et les droits d'accès au moins en lecture sur le formulaire ciblé lors de la connexion

